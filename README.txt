*****AUTOMAÇÃO CYPRESS - AMO PROMO*****
Este projeto consiste em uma automação end-to-end (E2E) desenvolvida utilizando o framework Cypress, com o objetivo de demonstrar habilidades em automação de testes para o site da empresa Amo Promo.

Descrição:
A automação E2E foi desenvolvida para testar os principais fluxos e funcionalidades do site da empresa Amo Promo, incluíndo compra de SEGURO VIAGEM com a funcionalidade pagamento por cartão de crédito, verificação se o cupom de desconto é válido para o SEGURO VIAGEM e compra de PASSAGEM AÉREA com a forma de pagamento PIX.

Pré-requisitos:
Antes de iniciar, certifique-se de ter o Node.js e o npm instalados na sua máquina

Instalação:
1) Clone o seguinte repositório para a sua máquina local:
https://gitlab.com/projeto_qa/amo-promo-qa.git

2) instale as dependências do projeto:
npm install

Executando os testes:
Para executar os testes utilize o seguinte comando:
npx cypress run >> para que os testes executem na linha de comando
npx cypress open >> para visualizar os testes na interface gráfica do Cypress






