Cypress.Commands.add('obterDatasFormatadas', () => {
  // Obter a data de hoje
  const hoje = new Date();

  // Calcular a data do dia seguinte
  const amanha = new Date(hoje);
  amanha.setDate(amanha.getDate() + 1); // Adicionar 1 dia

  // Calcular uma data alguns dias no futuro (por exemplo, 7 dias no futuro)
  const dataFutura = new Date(hoje);
  dataFutura.setDate(dataFutura.getDate() + 7); // Adicionar 7 dias

  // Formatar as datas no formato 'DD' para poder pesquisar

  const formatoData = { day: '2-digit' };
  const hojeFormatado = hoje.toLocaleDateString('pt-BR', formatoData);
  const amanhaFormatado = amanha.toLocaleDateString('pt-BR', formatoData);
  const dataFuturaFormatada = dataFutura.toLocaleDateString('pt-BR', formatoData);

  // Retornar um objeto com as datas formatadas
  return {
    hoje: hojeFormatado,
    amanha: amanhaFormatado,
    dataFutura: dataFuturaFormatada
  };
});

Cypress.Commands.add('preencherFormularioSeguro', (nome, sobrenome, cpf, email, telefone) => {
  cy.get('[data-cy="insured-0-name"]').click();
  cy.get('[data-cy="insured-0-name"]').type(`${nome} ${sobrenome}`);

  cy.get('[data-cy="insured-0-birthdate"]').click();
  cy.get('[data-cy="insured-0-birthdate"]').type('01/02/1990');

  cy.get('[data-cy="insured-0-cpf"]').click;
  cy.get('[data-cy="insured-0-cpf"]').type(cpf);

  cy.get('[data-cy="name"]').click();
  cy.get('[data-cy="name"]').type(`${nome} ${sobrenome}`)

  cy.get('[data-cy="email"]').click();
  cy.get('[data-cy="email"]').type(email);

  cy.get('[data-cy="phone"]').click();
  cy.get('[data-cy="phone"]').type(telefone);

  cy.get('[data-cy="cep"]').click()
  cy.get('[data-cy="cep"]').type('18048-005');

  cy.get('[data-cy="address"]').click();
  cy.get('[data-cy="address"]').type('Rua Ascencion Moreno Scudeler');

  cy.get('[data-cy="number"]').click();
  cy.get('[data-cy="number"]').type('100')

  cy.get('[data-cy="neighborhood"]').click();
  cy.get('[data-cy="neighborhood"]').type('Jardim Residencial Sunset Village');

  cy.get('[data-cy="city"]').click();
  cy.get('[data-cy="city"]').type('Sorocaba')

  cy.get('[data-cy="state"]').click();
  cy.contains('São Paulo').click();
})
Cypress.Commands.add('preencherFormularioPassagem', (nome, sobrenome, cpf, email, telefone) => {

  //Informe quem receberá o bilhete eletrônico
  cy.get('[data-cy="contactFullName"]').click();
  cy.get('[data-cy="contactFullName"]').type(`${nome} ${sobrenome}`)

  cy.get('[data-cy="contactEmail"]').click();
  cy.get('[data-cy="contactEmail"]').type(email);

  cy.get('[data-cy="contactPhone"]').click();
  cy.get('[data-cy="contactPhone"]').type(telefone);

  //Quem vai embarcar nessa viagem?
  cy.get('[data-cy="passenger0firstName"]').click();
  cy.get('[data-cy="passenger0firstName"]').type(nome);

  cy.get('[data-cy="passenger0lastName"]').click();
  cy.get('[data-cy="passenger0lastName"]').type(sobrenome)

  cy.get('[data-cy="passenger0document"]').click();
  cy.get('[data-cy="passenger0document"]').type(cpf);

  cy.get('[data-cy="passenger0birthDate"]').click();
  cy.get('[data-cy="passenger0birthDate"]').type('01/02/1990');

  cy.get('[data-cy="passenger0phone"]').click();
  cy.get('[data-cy="passenger0phone"]').type(telefone);

  cy.get('[data-cy="passenger0email"]').click();
  cy.get('[data-cy="passenger0email"]').type(email);

  cy.get('.vdp-ui-select-container').click();
  cy.contains('Feminino').click()
})
