///<reference types="cypress"/>
import faker from 'faker-br';

describe('Compra do Seguro Viagem com Cartão de Crédito', () => {
  let novaUrl;
  const nome = faker.name.firstName();
  const sobrenome = faker.name.lastName();
  const email = faker.internet.exampleEmail();
  const cpf = faker.br.cpf();
  const telefone = faker.phone.phoneNumber('######-####');

  it('Encontrar o Seguro Viagem', () => {
    //acessar a tela de pesquisa do seguro viagem
    cy.visit('/seguro-viagem/');

    /*Cypress.on('uncaught:exception', (err, runnable) => {
      // retornar false para evitar que o Cypress falhe o teste com o erro: Minified React error #423
      return false;
    });*/

    //clica no destino
    cy.get('[data-cy="destination"]').click();
    //procura na lista se tem algum destino com o nome de Brasil e clica
    cy.contains('Brasil').click();

    cy.get('[data-cy="departure"]').click();

    // Obter as datas formatadas
    cy.obterDatasFormatadas().then(({ amanha, dataFutura }) => {

      cy.get('[data-cy="calendar-month-days"]').contains(amanha).click();
      cy.get('[data-cy="calendar-month-days"]').contains(dataFutura).click();
    });

    //vou preencher o formulário com o nome, email e telefone
    cy.get('.styles_inputGroup__6EhnD [data-cy="name"]').click();
    cy.get('.styles_inputGroup__6EhnD [data-cy="name"]').type(`${nome} ${sobrenome}`);


    cy.get('.styles_inputGroup__6EhnD [data-cy="email"]').click();
    cy.get('.styles_inputGroup__6EhnD [data-cy="email"]').type(email);

    cy.get('[data-cy="phone"]').click();
    cy.get('[data-cy="phone"]').type(telefone);

    //vou clicar no botão para pesquisar
    cy.get('[data-cy="submit"]').click();

    //eu vou gravar a nova url que foi gerada para usar no próximo teste
    cy.url().then(url => {
      novaUrl = url;
    });
  })

  it('Selecionar o Seguro Viagem', () => {

    //visito a url que foi gerada no final no último código e verifico se os planos já estão visíveis
    cy.visit(novaUrl);
    cy.get('._plansListContainer_blsee_1 > :nth-child(2)').should('be.visible');

    //vou selecionar um plano e depois verificar se a próxima página está visível
    cy.get(':nth-child(2) > ._containerPrice_d5qut_1 > a > [data-cy="select-plan"]').click();
    cy.contains('Identificação das pessoas seguradas').should('be.visible');

    cy.url().then(url => {
      novaUrl = url;
    });
  })

  it('Verificar Cupom inválido', () => {
    cy.visit(novaUrl);
    cy.preencherFormularioSeguro(nome, sobrenome, cpf, email, telefone);

    cy.get('[data-cy="coupon"]').click();
    cy.get('[data-cy="coupon"]').type('amopromo');
    cy.get('[data-cy="button-coupon"]').click();

    cy.contains('Cupom inválido... Insira outro cupom!').should('be.visible');
  })

  it('Preencher Cartão de Crédito', () => {
    cy.visit(novaUrl);
    cy.preencherFormularioSeguro(nome, sobrenome, cpf, email, telefone);

    //seleciono o método cartão de crédito
    cy.get('[data-cy="method-creditcard"]').click();

    cy.get('[data-cy="payment-card"]').click();
    cy.get('[data-cy="payment-card"]').type('5162 1234 1234 1234');

    cy.get('[data-cy="payment-name"]').click();
    cy.get('[data-cy="payment-name"]').type(`${nome} ${sobrenome}`);

    cy.get('[data-cy="payment-cpf"]').click();
    cy.get('[data-cy="payment-cpf"]').type(cpf);

    cy.get('[data-cy="payment-date"]').click();
    cy.get('[data-cy="payment-date"]').type('12/2034');

    cy.get('[data-cy="payment-cvv"]').click();
    cy.get('[data-cy="payment-cvv"]').type('123');

    cy.get('[data-cy="select-parcels"]').click();
    cy.contains('1x sem juros').click()

    //Botão para finalizar o pagamento. Não está habilitado pois os testes estão sendo realizados no ambiente de produção.
    //cy.get('[data-cy="finalizar-pagamento"]').click();
    //cy.contains('Pedido recebido com sucesso!').should('be.visible');
    
  })

})