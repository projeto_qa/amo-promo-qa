///<reference types="cypress"/>
import faker from 'faker-br';

describe('Compra Passagem Aérea com PIX', () => {
    let novaUrl;
    const nome = faker.name.firstName();
    const sobrenome = faker.name.lastName();
    const email = faker.internet.exampleEmail();
    const cpf = faker.br.cpf();
    const telefone = faker.phone.phoneNumber('######-####');

    it('Pesquisar Passagem', () => {
        cy.visit('/passagens-aereas/');
        Cypress.on('uncaught:exception', (err, runnable) => {
            // retornar false para evitar que o Cypress falhe o teste com o erro: Minified React error #423
            return false;
        });

        //seleciona qual o lugar de partida
        cy.get('[data-cy="departure"]').click();
        cy.get('[data-cy="departure"]').type('Belo Horizonte');
        cy.get('[data-code="BHZ"]').click();

        //seleciona qual o destino
        cy.get('[data-cy="arrival"]').click();
        cy.get('[data-cy="arrival"]').type('São Paulo');
        cy.get('[data-cy="autocomplete-regular-item"][data-code="SAO"]').click();

        // Obter as datas formatadas
        cy.obterDatasFormatadas().then(({ amanha, dataFutura }) => {

            cy.get('[data-cy="departureDate"]').click();

            // Selecionar a data do dia seguinte ao hoje
            cy.get('[data-cy="calendar-month-days"]').contains(amanha).click();

            // Selecionar a data alguns dias no futuro
            cy.get('[data-cy="calendar-month-days"]').contains(dataFutura).click();

        });
        //Clico no botão de pesquisar
        cy.get('[data-cy="submitAirSearch"]').click();
        cy.wait(10000) //aguardo o carregamento da tela por completo
        cy.get('._container_i49m8_1').should('be.visible');

        //selecionar o botão de comprar
        cy.get(':nth-child(1) > ._content_1k4xb_1 > ._container_192le_1 > ._pricesContainer_192le_1 > ._buyButtonContainer_192le_175 > [data-cy="comprar-button"]').click();
        cy.wait(10000); //espero esse tempo para que a tela carregue por completo

        cy.url().then(url => {
            novaUrl = url;
        });

    })
    
    it('Preencher o formulário e seleciona o PIX', () => {
        cy.visit(novaUrl);
        cy.preencherFormularioPassagem(nome, sobrenome, cpf, email, telefone);

        cy.get('[data-cy="paymentSelectedPix"]').click();

        cy.get('[data-cy="paymentPixFullName"]').click();
        cy.get('[data-cy="paymentPixFullName"]').type(`${nome} ${sobrenome}`);

        cy.get('[data-cy="paymentPixDocument"]').click();
        cy.get('[data-cy="paymentPixDocument"]').type(cpf);

        cy.get('[data-cy="comprar-button"]').click();
        cy.wait(50000);

        cy.get('._statusTitle_pqiyq_20').should('be.visible');
        cy.contains('Pedido recebido');
    })
})